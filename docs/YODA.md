# YODA (YOur DAta)

## What is YODA?

YODA is a data management tool developed by Utrecht University to act as an visual data management interface _on top of_ iRods. It allows metadata editing at a directory level, according to standard templates. It has two main areas: a _research_ area, which is where active research data can be stored. This is the environment that you interact with through iRods directly or through your file explorer. The other area is the _Vault_. This is an area that acts as a readonly archive: finalised results are staged from the active research area to go to the vault pending approval by a data steward. After approval, they are archived in the Vault; any changes made thereafter will result in a new vault item. Items in the vault are referred to as research data packages.

## How to get started with YODA at WUR?

An account will first be made for you by an administrator. Details for this will be sent to you via email, to allow your first login. **Check your spam folder for this message!**

After you've received an email that your account has been created, go to https://wur-yoda.irods.surfsara.nl and use the "Sign in" button to sign in. To see what data is currently present under your project folder, select "Research" from the four main tabs.

Next, you will want to do a few things:
> Note: Use https://wur-data.irods.surfsara.nl as the hostname in all UU instructions.
 - Uploading data: this can be done with the upload button on the YODA website, or by mounting the research area as a drive within your operating system. See [mount instructions on the UU website](https://yoda.uu.nl/yoda-uu-nl/getting-started/index.html) to do this.

 - Managing metadata: once you've uploaded files, you cvan start adding metadata to them. This can be done with forms through YODA webportal ([instructions on the UU website](https://yoda.uu.nl/yoda-uu-nl/functions/Add-metadata.html)). It is also possible to do this using the iCommands, see more detailed instructions.

 - Archive data in the vault: Once you have uploaded and annotated data and you are reasonably sure the data will not change, it can be stored securely in the Vault. See [Vault instructions at the UU website](https://yoda.uu.nl/yoda-uu-nl/functions/Archiving.html) to do this.

 - (Available soon) Publishing your data: In addition to storing data in the Vault, data can at that point be published and reveive a DOI so that it can be referenced and cited. See [more detailed publishing instructions](https://yoda.uu.nl/yoda-uu-nl/functions/Publishing.html).

 Finally, please consult the FAQ document in order to solve any issues you might have. If your answer is not listed, send an email to [data@wur.nl](mailto:data@wur.nl?subject=Question%20about%20YODA%20).
