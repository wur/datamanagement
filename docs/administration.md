## User management

### User creation

=== "iCommands"

Users can be created using `iadmin mkuser Name[#Zone] Type (make user)` in which the Name corresponds to the username to be created and the user type is one of the types available under `iadmin lt user_type`.

=== "Python API"

    ``` sh
    ....
    ```

#### User removal

=== "iCommands"

To remove a user `rmuser Name[#Zone]`




### Group management
